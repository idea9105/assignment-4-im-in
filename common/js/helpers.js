


$(document).ready(function() {



$('#filters select').change(function() {
	 $('.eventResult').fadeOut();
	 $('.eventResult:eq(2)').fadeIn();
});
 



if ( $('[type="date"]').prop('type') != 'date' ) {
	
	$('[type="date"]').focus(function() {
		$(this).siblings('.ui-datepicker-calendar').fadeIn();
	});

	$('.ui-datepicker-calendar a').addClass('notBuilt');


	$('[type="date"]').blur(function() {
		$(this).siblings('.ui-datepicker-calendar').fadeOut();
		$(this).val("15/06/2016");
	});

}

if ( $('[type="time"]').prop('type') != 'time' ) {
	
	$('[type="time"]').addClass('notBuilt');

}

// Give feedback about characters used and limit on a field

$(".button-im-in").click(function(e) {
	if($(this).hasClass('selected')) {
		$(this).removeClass("selected");
		if($(this).text()=="I'm going!") {
			$(this).text("Attend");
		}
	} else {
		if($(this).text()=="Attend") {
			$(this).text("I'm going!");
		}
		$(this).addClass("selected");
	}
	e.preventDefault();
});


// Actions for resource selector modal

$('#resourceSelector .active li ').click(function(e) {
	if($(this).hasClass('selected')) {
		$(this).siblings().show();
		$(this).children("ul").hide();
		$(this).removeClass('selected');
		$(this).show();
		$(this).children("ul").removeClass('active');
		   e.stopPropagation();

	} else {
		$(this).siblings().hide();
		$(this).children().show();
		$(this).addClass('selected');
		$(this).children("ul").addClass('active');
		$(this).parent("ul").removeClass('active');
		   e.stopPropagation();

	}
});

$('#modalAddResource .button').click(function(e) {
	$('.interactiveReveal').fadeIn();
	$('main').removeClass("dimmed");
});


$(".light-box").click(function(e) {
		$('main').addClass("dimmed");
	e.stopPropagation();
});

$('main.dimmed').click(function() {
		$('main').removeClass("dimmed");
	});


$("#back").click(function(e) {
    parent.history.back();
    return false;             
});
    

// Actions for Create Event page

$("#event_title").focus();

$("#event_title").keydown(function() {
		$('#create-event-title label').hide();	
	});

$("#event_title").keyup(function() {
		var n = $(this).val().length;
		var max = $(this).attr("maxlength");
		$(this).siblings(".helper-text" ).text( n + "/" + max);
		if(n<1) {
			$('#create-event-title label').fadeIn();	
		}
	});


$("#createFromTemplate input").change(function() {
	$(this).parent("label").addClass('checked');
	$(this).parent("label").siblings().removeClass('checked');
	});

if(window.location.hash) {
	$("input[value='bbq']").prop("checked", true);
	$("input[value='bbq']").parent("label").addClass('checked');
	$('#resources-needed').fadeIn();
	$('#event_description').val("Join us for a fun day in the park with live music and great food. We are looking for individuals or groups who can help us in welcoming the newest members of our community.");
	$('.button.disabled').removeClass('disabled');
}

$("input[value='bbq']").change(function() {
	$('#resources-needed').fadeIn();
	$('#event_description').val("Join us for a fun day in the park with live music and great food. We are looking for individuals or groups who can help us in welcoming the newest members of our community.");
	$('.button.disabled').removeClass('disabled');
});




// $(function() {
//   $('#imin').smoothState();
// });


// $(function(){
//   'use strict';
//   var $page = $('#imin'),
//       options = {
//         debug: true,
//         prefetch: true,
//         cacheLength: 2,
//         onStart: {
//           duration: 250, // Duration of our animation
//           render: function ($container) {
//             // Add your CSS animation reversing class
//             $container.addClass('animated');
//             // Restart your animation
//             smoothState.restartCSSAnimations();
//           }
//         },
//         onReady: {
//           duration: 0,
//           render: function ($container, $newContent) {
//             // Remove your CSS animation reversing class
//             $container.removeClass('is-exiting');
//             // Inject the new content
//             $container.html($newContent);
//           }
//         }
//       },
//       smoothState = $page.smoothState(options).data('smoothState');
// });


});		